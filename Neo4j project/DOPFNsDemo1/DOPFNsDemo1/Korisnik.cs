﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOPFNsDemo1
{
    public class Korisnik
    {
        public string Name { get; set; }
        public string Age { get; set; }
        public string Pol { get; set; }
        public string Id { get; set; }
        public string Password { get; set; }
    }
}
