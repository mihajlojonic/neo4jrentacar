﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOPFNsDemo1
{
    public class KorisnikTabela
    {
        public Korisnik user = new Korisnik();
        string followers;
        string following;
        bool isActive;
        bool alreadyFollow;

        public Korisnik User { get => user; set => user = value; }
        public string Followers { get => followers; set => followers = value; }
        public string Following { get => following; set => following = value; }
        public bool IsActive { get => isActive; set => isActive = value; }
        public bool AlreadyFollow { get => alreadyFollow; set => alreadyFollow = value; }

        public KorisnikTabela()
        {
            //user = new Korisnik();
        }
    }
}
