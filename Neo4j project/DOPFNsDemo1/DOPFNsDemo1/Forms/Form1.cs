﻿using DOPFNsDemo1.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DOPFNsDemo1
{
    public partial class Form1 : Form
    {
        private static Korisnik newUser;
        DB dbInstance = DB.Intance();
        Random rnd = new Random();
        RC4 rc4 = new RC4();

        internal static Korisnik NewUser { get => newUser; set => newUser = value; }

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void bunifuThinButton21_Click(object sender, EventArgs e)
        {
            if (dbInstance.checkUser(this.txtUserName.Text, this.txtPassword.Text))
            {
                newUser = dbInstance.getUserByUsernamePassword(this.txtUserName.Text, this.txtPassword.Text);
                formDashboard obj = new formDashboard(NewUser);//promeni
                obj.Show();
                this.Hide();
            }
            else
                label2.Text = "Try again";

        }

        private void btnSignIn_Click(object sender, EventArgs e)
        {
            string username = this.txtUserName.Text;
            string password = this.txtPassword.Text;
            string userID = rnd.Next(1, 10).ToString() + rc4.Crypt(DateTime.Now.Ticks.ToString());

            if (username.Length < 4 || password.Length < 4) return;


            if(dbInstance.createUser(username, password, userID))
            {
                NewUser = dbInstance.getUserById(userID);
                questionsControl.Visible = true;
                questionsControl.BringToFront();
            }
            else
            {
                label2.Text = "Try again";
            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            questionsControl.Visible = false;
        }

        
    }
}
