﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DOPFNsDemo1.Forms
{
    public partial class formDashboard : Form
    {
        Korisnik user;
        DB dbInstance = DB.Intance();

        public Korisnik User { get => user; set => user = value; }

        public formDashboard(Korisnik user)
        {
            InitializeComponent();
            usersTableUC1.BringToFront();
            labelTitle.Text = "Dashboard";

            this.User = user;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            usersTableUC1.BringToFront();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            carsTableUC1.BringToFront();
        }

        private void donersUC1_Load(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            dbInstance.Active(false, User.Id);
            Application.Exit();
        }

        private void formDashboard_Load(object sender, EventArgs e)
        {
            this.lblHi.Text = "Hi, " + User.Name;
            dbInstance.Active(true, User.Id);
        }

        private void usersTableUC1_Load(object sender, EventArgs e)
        {
            KorisnikTabela[] allUsers = dbInstance.getUsersForTable(Form1.NewUser.Id);
            this.usersTableUC1.SetUsers(allUsers);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            recommTable1.BringToFront();
        }

        private void recommTable1_Load(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            likeUCUC1.BringToFront();
        }
    }
}
