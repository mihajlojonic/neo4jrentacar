﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOPFNsDemo1
{
    public class RecommandedRentacar
    {
        Rentacar rent;
        string numberOfFriends;
        string numberOfCars;

        public string NumberOfFriends { get => numberOfFriends; set => numberOfFriends = value; }
        internal Rentacar Rent { get => rent; set => rent = value; }

        public RecommandedRentacar() { Rent = new Rentacar(); }
    }
}
