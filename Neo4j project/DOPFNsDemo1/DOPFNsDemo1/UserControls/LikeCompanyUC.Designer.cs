﻿namespace DOPFNsDemo1.UserControls
{
    partial class LikeCompanyUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblCompany2 = new System.Windows.Forms.Label();
            this.lblCompany3 = new System.Windows.Forms.Label();
            this.lblCompany6 = new System.Windows.Forms.Label();
            this.lblCompany4 = new System.Windows.Forms.Label();
            this.lblCompany5 = new System.Windows.Forms.Label();
            this.lblCompany1 = new System.Windows.Forms.Label();
            this.btnLike1 = new System.Windows.Forms.Button();
            this.btnLike2 = new System.Windows.Forms.Button();
            this.btnLike3 = new System.Windows.Forms.Button();
            this.btnLike4 = new System.Windows.Forms.Button();
            this.btnLike5 = new System.Windows.Forms.Button();
            this.btnLike6 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblCompany2
            // 
            this.lblCompany2.AutoSize = true;
            this.lblCompany2.Font = new System.Drawing.Font("Broadway", 16.2F);
            this.lblCompany2.ForeColor = System.Drawing.Color.DimGray;
            this.lblCompany2.Location = new System.Drawing.Point(12, 87);
            this.lblCompany2.Name = "lblCompany2";
            this.lblCompany2.Size = new System.Drawing.Size(180, 32);
            this.lblCompany2.TabIndex = 28;
            this.lblCompany2.Text = "Company2";
            // 
            // lblCompany3
            // 
            this.lblCompany3.AutoSize = true;
            this.lblCompany3.Font = new System.Drawing.Font("Broadway", 16.2F);
            this.lblCompany3.ForeColor = System.Drawing.Color.DimGray;
            this.lblCompany3.Location = new System.Drawing.Point(12, 154);
            this.lblCompany3.Name = "lblCompany3";
            this.lblCompany3.Size = new System.Drawing.Size(180, 32);
            this.lblCompany3.TabIndex = 27;
            this.lblCompany3.Text = "Company3";
            // 
            // lblCompany6
            // 
            this.lblCompany6.AutoSize = true;
            this.lblCompany6.Font = new System.Drawing.Font("Broadway", 16.2F);
            this.lblCompany6.ForeColor = System.Drawing.Color.DimGray;
            this.lblCompany6.Location = new System.Drawing.Point(310, 154);
            this.lblCompany6.Name = "lblCompany6";
            this.lblCompany6.Size = new System.Drawing.Size(180, 32);
            this.lblCompany6.TabIndex = 26;
            this.lblCompany6.Text = "Company6";
            // 
            // lblCompany4
            // 
            this.lblCompany4.AutoSize = true;
            this.lblCompany4.Font = new System.Drawing.Font("Broadway", 16.2F);
            this.lblCompany4.ForeColor = System.Drawing.Color.DimGray;
            this.lblCompany4.Location = new System.Drawing.Point(310, 26);
            this.lblCompany4.Name = "lblCompany4";
            this.lblCompany4.Size = new System.Drawing.Size(180, 32);
            this.lblCompany4.TabIndex = 25;
            this.lblCompany4.Text = "Company4";
            // 
            // lblCompany5
            // 
            this.lblCompany5.AutoSize = true;
            this.lblCompany5.Font = new System.Drawing.Font("Broadway", 16.2F);
            this.lblCompany5.ForeColor = System.Drawing.Color.DimGray;
            this.lblCompany5.Location = new System.Drawing.Point(310, 87);
            this.lblCompany5.Name = "lblCompany5";
            this.lblCompany5.Size = new System.Drawing.Size(180, 32);
            this.lblCompany5.TabIndex = 24;
            this.lblCompany5.Text = "Company5";
            // 
            // lblCompany1
            // 
            this.lblCompany1.AutoSize = true;
            this.lblCompany1.Font = new System.Drawing.Font("Broadway", 16.2F);
            this.lblCompany1.ForeColor = System.Drawing.Color.DimGray;
            this.lblCompany1.Location = new System.Drawing.Point(12, 26);
            this.lblCompany1.Name = "lblCompany1";
            this.lblCompany1.Size = new System.Drawing.Size(180, 32);
            this.lblCompany1.TabIndex = 23;
            this.lblCompany1.Text = "Company1";
            // 
            // btnLike1
            // 
            this.btnLike1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(0)))), ((int)(((byte)(127)))));
            this.btnLike1.ForeColor = System.Drawing.Color.White;
            this.btnLike1.Location = new System.Drawing.Point(217, 26);
            this.btnLike1.Name = "btnLike1";
            this.btnLike1.Size = new System.Drawing.Size(75, 32);
            this.btnLike1.TabIndex = 29;
            this.btnLike1.Text = "Like";
            this.btnLike1.UseVisualStyleBackColor = false;
            this.btnLike1.Click += new System.EventHandler(this.btnLike1_Click_1);
            // 
            // btnLike2
            // 
            this.btnLike2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(0)))), ((int)(((byte)(127)))));
            this.btnLike2.ForeColor = System.Drawing.Color.White;
            this.btnLike2.Location = new System.Drawing.Point(217, 87);
            this.btnLike2.Name = "btnLike2";
            this.btnLike2.Size = new System.Drawing.Size(75, 32);
            this.btnLike2.TabIndex = 30;
            this.btnLike2.Text = "Like";
            this.btnLike2.UseVisualStyleBackColor = false;
            this.btnLike2.Click += new System.EventHandler(this.btnLike2_Click_1);
            // 
            // btnLike3
            // 
            this.btnLike3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(0)))), ((int)(((byte)(127)))));
            this.btnLike3.ForeColor = System.Drawing.Color.White;
            this.btnLike3.Location = new System.Drawing.Point(217, 154);
            this.btnLike3.Name = "btnLike3";
            this.btnLike3.Size = new System.Drawing.Size(75, 32);
            this.btnLike3.TabIndex = 31;
            this.btnLike3.Text = "Like";
            this.btnLike3.UseVisualStyleBackColor = false;
            this.btnLike3.Click += new System.EventHandler(this.btnLike3_Click_1);
            // 
            // btnLike4
            // 
            this.btnLike4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(0)))), ((int)(((byte)(127)))));
            this.btnLike4.ForeColor = System.Drawing.Color.White;
            this.btnLike4.Location = new System.Drawing.Point(496, 30);
            this.btnLike4.Name = "btnLike4";
            this.btnLike4.Size = new System.Drawing.Size(75, 32);
            this.btnLike4.TabIndex = 32;
            this.btnLike4.Text = "Like";
            this.btnLike4.UseVisualStyleBackColor = false;
            this.btnLike4.Click += new System.EventHandler(this.btnLike4_Click_1);
            // 
            // btnLike5
            // 
            this.btnLike5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(0)))), ((int)(((byte)(127)))));
            this.btnLike5.ForeColor = System.Drawing.Color.White;
            this.btnLike5.Location = new System.Drawing.Point(496, 87);
            this.btnLike5.Name = "btnLike5";
            this.btnLike5.Size = new System.Drawing.Size(75, 32);
            this.btnLike5.TabIndex = 33;
            this.btnLike5.Text = "Like";
            this.btnLike5.UseVisualStyleBackColor = false;
            this.btnLike5.Click += new System.EventHandler(this.btnLike5_Click_1);
            // 
            // btnLike6
            // 
            this.btnLike6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(0)))), ((int)(((byte)(127)))));
            this.btnLike6.ForeColor = System.Drawing.Color.White;
            this.btnLike6.Location = new System.Drawing.Point(496, 154);
            this.btnLike6.Name = "btnLike6";
            this.btnLike6.Size = new System.Drawing.Size(75, 32);
            this.btnLike6.TabIndex = 34;
            this.btnLike6.Text = "Like";
            this.btnLike6.UseVisualStyleBackColor = false;
            this.btnLike6.Click += new System.EventHandler(this.btnLike6_Click_1);
            // 
            // LikeCompanyUC
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.btnLike6);
            this.Controls.Add(this.btnLike5);
            this.Controls.Add(this.btnLike4);
            this.Controls.Add(this.btnLike3);
            this.Controls.Add(this.btnLike2);
            this.Controls.Add(this.btnLike1);
            this.Controls.Add(this.lblCompany2);
            this.Controls.Add(this.lblCompany3);
            this.Controls.Add(this.lblCompany6);
            this.Controls.Add(this.lblCompany4);
            this.Controls.Add(this.lblCompany5);
            this.Controls.Add(this.lblCompany1);
            this.Name = "LikeCompanyUC";
            this.Size = new System.Drawing.Size(596, 202);
            this.Load += new System.EventHandler(this.LikeCompanyUC_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblCompany2;
        private System.Windows.Forms.Label lblCompany3;
        private System.Windows.Forms.Label lblCompany6;
        private System.Windows.Forms.Label lblCompany4;
        private System.Windows.Forms.Label lblCompany5;
        private System.Windows.Forms.Label lblCompany1;
        private System.Windows.Forms.Button btnLike1;
        private System.Windows.Forms.Button btnLike2;
        private System.Windows.Forms.Button btnLike3;
        private System.Windows.Forms.Button btnLike4;
        private System.Windows.Forms.Button btnLike5;
        private System.Windows.Forms.Button btnLike6;
    }
}
