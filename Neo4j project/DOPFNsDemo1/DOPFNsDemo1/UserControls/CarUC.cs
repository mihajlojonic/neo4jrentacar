﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DOPFNsDemo1.UserControls
{
    public partial class CarUC : UserControl
    {
        Rentacar rent = new Rentacar();
        string idAutomobila;

        public CarUC()
        {
            InitializeComponent();
        }

        private void CarUC_Load(object sender, EventArgs e)
        {

        }

        internal void setCarData(CarTabela carTabela)
        {
            this.idAutomobila = carTabela.Car.Id;

            this.lblMarka.Text = carTabela.Marka;
            this.lblModel.Text = carTabela.Car.Model;
            this.lblPotrosnja.Text = carTabela.Car.Potrosnja;
            this.lblSnaga.Text = carTabela.Car.Snaga;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //dateTimePicker1.Value.ToString();
            DB.Intance().createRelationshipIznajmljivanje(Form1.NewUser.Id, rent.Id, idAutomobila, dateTimePicker1.Value.ToString());
            button1.Enabled = false;
        }

        internal void setRentcar(Rentacar rentacar)
        {
            this.rent = rentacar;
        }
    }
}
