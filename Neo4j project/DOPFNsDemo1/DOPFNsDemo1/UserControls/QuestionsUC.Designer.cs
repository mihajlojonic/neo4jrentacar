﻿namespace DOPFNsDemo1.UserControls
{
    partial class QuestionsUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QuestionsUC));
            this.btnLike1 = new Bunifu.Framework.UI.BunifuThinButton2();
            this.btnLike2 = new Bunifu.Framework.UI.BunifuThinButton2();
            this.btnLike3 = new Bunifu.Framework.UI.BunifuThinButton2();
            this.btnLike4 = new Bunifu.Framework.UI.BunifuThinButton2();
            this.btnLike6 = new Bunifu.Framework.UI.BunifuThinButton2();
            this.btnLike5 = new Bunifu.Framework.UI.BunifuThinButton2();
            this.lblCompany1 = new System.Windows.Forms.Label();
            this.lblCompany5 = new System.Windows.Forms.Label();
            this.lblCompany4 = new System.Windows.Forms.Label();
            this.lblCompany6 = new System.Windows.Forms.Label();
            this.lblCompany3 = new System.Windows.Forms.Label();
            this.lblCompany2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnLike1
            // 
            this.btnLike1.ActiveBorderThickness = 1;
            this.btnLike1.ActiveCornerRadius = 20;
            this.btnLike1.ActiveFillColor = System.Drawing.Color.Blue;
            this.btnLike1.ActiveForecolor = System.Drawing.Color.White;
            this.btnLike1.ActiveLineColor = System.Drawing.Color.Blue;
            this.btnLike1.BackColor = System.Drawing.Color.White;
            this.btnLike1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnLike1.BackgroundImage")));
            this.btnLike1.ButtonText = "Like";
            this.btnLike1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLike1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLike1.ForeColor = System.Drawing.Color.White;
            this.btnLike1.IdleBorderThickness = 1;
            this.btnLike1.IdleCornerRadius = 20;
            this.btnLike1.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(0)))), ((int)(((byte)(127)))));
            this.btnLike1.IdleForecolor = System.Drawing.Color.White;
            this.btnLike1.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(0)))), ((int)(((byte)(127)))));
            this.btnLike1.Location = new System.Drawing.Point(223, 12);
            this.btnLike1.Margin = new System.Windows.Forms.Padding(5);
            this.btnLike1.Name = "btnLike1";
            this.btnLike1.Size = new System.Drawing.Size(73, 42);
            this.btnLike1.TabIndex = 5;
            this.btnLike1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnLike1.Click += new System.EventHandler(this.btnLike1_Click);
            // 
            // btnLike2
            // 
            this.btnLike2.ActiveBorderThickness = 1;
            this.btnLike2.ActiveCornerRadius = 20;
            this.btnLike2.ActiveFillColor = System.Drawing.Color.Blue;
            this.btnLike2.ActiveForecolor = System.Drawing.Color.White;
            this.btnLike2.ActiveLineColor = System.Drawing.Color.Blue;
            this.btnLike2.BackColor = System.Drawing.Color.White;
            this.btnLike2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnLike2.BackgroundImage")));
            this.btnLike2.ButtonText = "Like";
            this.btnLike2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLike2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLike2.ForeColor = System.Drawing.Color.White;
            this.btnLike2.IdleBorderThickness = 1;
            this.btnLike2.IdleCornerRadius = 20;
            this.btnLike2.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(0)))), ((int)(((byte)(127)))));
            this.btnLike2.IdleForecolor = System.Drawing.Color.White;
            this.btnLike2.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(0)))), ((int)(((byte)(127)))));
            this.btnLike2.Location = new System.Drawing.Point(223, 82);
            this.btnLike2.Margin = new System.Windows.Forms.Padding(5);
            this.btnLike2.Name = "btnLike2";
            this.btnLike2.Size = new System.Drawing.Size(73, 42);
            this.btnLike2.TabIndex = 6;
            this.btnLike2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnLike3
            // 
            this.btnLike3.ActiveBorderThickness = 1;
            this.btnLike3.ActiveCornerRadius = 20;
            this.btnLike3.ActiveFillColor = System.Drawing.Color.Blue;
            this.btnLike3.ActiveForecolor = System.Drawing.Color.White;
            this.btnLike3.ActiveLineColor = System.Drawing.Color.Blue;
            this.btnLike3.BackColor = System.Drawing.Color.White;
            this.btnLike3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnLike3.BackgroundImage")));
            this.btnLike3.ButtonText = "Like";
            this.btnLike3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLike3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLike3.ForeColor = System.Drawing.Color.White;
            this.btnLike3.IdleBorderThickness = 1;
            this.btnLike3.IdleCornerRadius = 20;
            this.btnLike3.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(0)))), ((int)(((byte)(127)))));
            this.btnLike3.IdleForecolor = System.Drawing.Color.White;
            this.btnLike3.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(0)))), ((int)(((byte)(127)))));
            this.btnLike3.Location = new System.Drawing.Point(223, 139);
            this.btnLike3.Margin = new System.Windows.Forms.Padding(5);
            this.btnLike3.Name = "btnLike3";
            this.btnLike3.Size = new System.Drawing.Size(73, 42);
            this.btnLike3.TabIndex = 7;
            this.btnLike3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnLike4
            // 
            this.btnLike4.ActiveBorderThickness = 1;
            this.btnLike4.ActiveCornerRadius = 20;
            this.btnLike4.ActiveFillColor = System.Drawing.Color.Blue;
            this.btnLike4.ActiveForecolor = System.Drawing.Color.White;
            this.btnLike4.ActiveLineColor = System.Drawing.Color.Blue;
            this.btnLike4.BackColor = System.Drawing.Color.White;
            this.btnLike4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnLike4.BackgroundImage")));
            this.btnLike4.ButtonText = "Like";
            this.btnLike4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLike4.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLike4.ForeColor = System.Drawing.Color.White;
            this.btnLike4.IdleBorderThickness = 1;
            this.btnLike4.IdleCornerRadius = 20;
            this.btnLike4.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(0)))), ((int)(((byte)(127)))));
            this.btnLike4.IdleForecolor = System.Drawing.Color.White;
            this.btnLike4.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(0)))), ((int)(((byte)(127)))));
            this.btnLike4.Location = new System.Drawing.Point(518, 12);
            this.btnLike4.Margin = new System.Windows.Forms.Padding(5);
            this.btnLike4.Name = "btnLike4";
            this.btnLike4.Size = new System.Drawing.Size(73, 42);
            this.btnLike4.TabIndex = 8;
            this.btnLike4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnLike6
            // 
            this.btnLike6.ActiveBorderThickness = 1;
            this.btnLike6.ActiveCornerRadius = 20;
            this.btnLike6.ActiveFillColor = System.Drawing.Color.Blue;
            this.btnLike6.ActiveForecolor = System.Drawing.Color.White;
            this.btnLike6.ActiveLineColor = System.Drawing.Color.Blue;
            this.btnLike6.BackColor = System.Drawing.Color.White;
            this.btnLike6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnLike6.BackgroundImage")));
            this.btnLike6.ButtonText = "Like";
            this.btnLike6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLike6.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLike6.ForeColor = System.Drawing.Color.White;
            this.btnLike6.IdleBorderThickness = 1;
            this.btnLike6.IdleCornerRadius = 20;
            this.btnLike6.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(0)))), ((int)(((byte)(127)))));
            this.btnLike6.IdleForecolor = System.Drawing.Color.White;
            this.btnLike6.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(0)))), ((int)(((byte)(127)))));
            this.btnLike6.Location = new System.Drawing.Point(518, 139);
            this.btnLike6.Margin = new System.Windows.Forms.Padding(5);
            this.btnLike6.Name = "btnLike6";
            this.btnLike6.Size = new System.Drawing.Size(73, 42);
            this.btnLike6.TabIndex = 9;
            this.btnLike6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnLike5
            // 
            this.btnLike5.ActiveBorderThickness = 1;
            this.btnLike5.ActiveCornerRadius = 20;
            this.btnLike5.ActiveFillColor = System.Drawing.Color.Blue;
            this.btnLike5.ActiveForecolor = System.Drawing.Color.White;
            this.btnLike5.ActiveLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(0)))), ((int)(((byte)(127)))));
            this.btnLike5.BackColor = System.Drawing.Color.White;
            this.btnLike5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnLike5.BackgroundImage")));
            this.btnLike5.ButtonText = "Like";
            this.btnLike5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLike5.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLike5.ForeColor = System.Drawing.Color.White;
            this.btnLike5.IdleBorderThickness = 1;
            this.btnLike5.IdleCornerRadius = 20;
            this.btnLike5.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(0)))), ((int)(((byte)(127)))));
            this.btnLike5.IdleForecolor = System.Drawing.Color.White;
            this.btnLike5.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(0)))), ((int)(((byte)(127)))));
            this.btnLike5.Location = new System.Drawing.Point(518, 73);
            this.btnLike5.Margin = new System.Windows.Forms.Padding(5);
            this.btnLike5.Name = "btnLike5";
            this.btnLike5.Size = new System.Drawing.Size(73, 42);
            this.btnLike5.TabIndex = 10;
            this.btnLike5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCompany1
            // 
            this.lblCompany1.AutoSize = true;
            this.lblCompany1.Font = new System.Drawing.Font("Broadway", 16.2F);
            this.lblCompany1.ForeColor = System.Drawing.Color.DimGray;
            this.lblCompany1.Location = new System.Drawing.Point(18, 21);
            this.lblCompany1.Name = "lblCompany1";
            this.lblCompany1.Size = new System.Drawing.Size(180, 32);
            this.lblCompany1.TabIndex = 11;
            this.lblCompany1.Text = "Company1";
            // 
            // lblCompany5
            // 
            this.lblCompany5.AutoSize = true;
            this.lblCompany5.Font = new System.Drawing.Font("Broadway", 16.2F);
            this.lblCompany5.ForeColor = System.Drawing.Color.DimGray;
            this.lblCompany5.Location = new System.Drawing.Point(316, 82);
            this.lblCompany5.Name = "lblCompany5";
            this.lblCompany5.Size = new System.Drawing.Size(180, 32);
            this.lblCompany5.TabIndex = 12;
            this.lblCompany5.Text = "Company5";
            // 
            // lblCompany4
            // 
            this.lblCompany4.AutoSize = true;
            this.lblCompany4.Font = new System.Drawing.Font("Broadway", 16.2F);
            this.lblCompany4.ForeColor = System.Drawing.Color.DimGray;
            this.lblCompany4.Location = new System.Drawing.Point(316, 21);
            this.lblCompany4.Name = "lblCompany4";
            this.lblCompany4.Size = new System.Drawing.Size(180, 32);
            this.lblCompany4.TabIndex = 13;
            this.lblCompany4.Text = "Company4";
            // 
            // lblCompany6
            // 
            this.lblCompany6.AutoSize = true;
            this.lblCompany6.Font = new System.Drawing.Font("Broadway", 16.2F);
            this.lblCompany6.ForeColor = System.Drawing.Color.DimGray;
            this.lblCompany6.Location = new System.Drawing.Point(316, 149);
            this.lblCompany6.Name = "lblCompany6";
            this.lblCompany6.Size = new System.Drawing.Size(180, 32);
            this.lblCompany6.TabIndex = 14;
            this.lblCompany6.Text = "Company6";
            // 
            // lblCompany3
            // 
            this.lblCompany3.AutoSize = true;
            this.lblCompany3.Font = new System.Drawing.Font("Broadway", 16.2F);
            this.lblCompany3.ForeColor = System.Drawing.Color.DimGray;
            this.lblCompany3.Location = new System.Drawing.Point(18, 149);
            this.lblCompany3.Name = "lblCompany3";
            this.lblCompany3.Size = new System.Drawing.Size(180, 32);
            this.lblCompany3.TabIndex = 15;
            this.lblCompany3.Text = "Company3";
            // 
            // lblCompany2
            // 
            this.lblCompany2.AutoSize = true;
            this.lblCompany2.Font = new System.Drawing.Font("Broadway", 16.2F);
            this.lblCompany2.ForeColor = System.Drawing.Color.DimGray;
            this.lblCompany2.Location = new System.Drawing.Point(18, 82);
            this.lblCompany2.Name = "lblCompany2";
            this.lblCompany2.Size = new System.Drawing.Size(180, 32);
            this.lblCompany2.TabIndex = 16;
            this.lblCompany2.Text = "Company2";
            // 
            // QuestionsUC
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.lblCompany2);
            this.Controls.Add(this.lblCompany3);
            this.Controls.Add(this.lblCompany6);
            this.Controls.Add(this.lblCompany4);
            this.Controls.Add(this.lblCompany5);
            this.Controls.Add(this.lblCompany1);
            this.Controls.Add(this.btnLike5);
            this.Controls.Add(this.btnLike6);
            this.Controls.Add(this.btnLike4);
            this.Controls.Add(this.btnLike3);
            this.Controls.Add(this.btnLike2);
            this.Controls.Add(this.btnLike1);
            this.Name = "QuestionsUC";
            this.Size = new System.Drawing.Size(596, 202);
            this.Load += new System.EventHandler(this.QuestionsUC_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuThinButton2 btnLike1;
        private Bunifu.Framework.UI.BunifuThinButton2 btnLike2;
        private Bunifu.Framework.UI.BunifuThinButton2 btnLike3;
        private Bunifu.Framework.UI.BunifuThinButton2 btnLike4;
        private Bunifu.Framework.UI.BunifuThinButton2 btnLike6;
        private Bunifu.Framework.UI.BunifuThinButton2 btnLike5;
        private System.Windows.Forms.Label lblCompany1;
        private System.Windows.Forms.Label lblCompany5;
        private System.Windows.Forms.Label lblCompany4;
        private System.Windows.Forms.Label lblCompany6;
        private System.Windows.Forms.Label lblCompany3;
        private System.Windows.Forms.Label lblCompany2;
    }
}
