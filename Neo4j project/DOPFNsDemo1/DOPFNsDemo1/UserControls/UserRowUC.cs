﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DOPFNsDemo1.Forms;

namespace DOPFNsDemo1.UserControls
{
    public partial class UserRowUC : UserControl
    {


        public UserRowUC()
        {
            InitializeComponent();
        }

        internal void SetUsers(KorisnikTabela korisnikTabela)
        {
            this.lblName.Text = korisnikTabela.User.Name;
            this.lblAge.Text = korisnikTabela.User.Age;
            this.button1.Tag = korisnikTabela.User.Id;
            this.lblFollowersNum.Text = korisnikTabela.Followers;
            this.lblFollowingNum.Text = korisnikTabela.Following;

            if(korisnikTabela.IsActive == true)
                this.panelActive.BackColor = Color.Blue;
            else
                this.panelActive.BackColor = Color.Red;

            if (korisnikTabela.AlreadyFollow == true)
                this.button1.Enabled = false;
            else
                this.button1.Enabled = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Form1.NewUser.Id
            DB.Intance().createRelationshipPrati(Form1.NewUser.Id, button1.Tag.ToString());
            button1.Enabled = false;
        }

        private void UserRowUC_Load(object sender, EventArgs e)
        {

        }
    }
}
