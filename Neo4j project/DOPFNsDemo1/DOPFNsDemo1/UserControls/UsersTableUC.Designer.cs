﻿namespace DOPFNsDemo1.UserControls
{
    partial class UsersTableUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblTableIsActive = new System.Windows.Forms.Label();
            this.lblTableFollowers = new System.Windows.Forms.Label();
            this.lblTableFollowing = new System.Windows.Forms.Label();
            this.lblTableAge = new System.Windows.Forms.Label();
            this.lblTableUsername = new System.Windows.Forms.Label();
            this.btnPrevPage = new System.Windows.Forms.Button();
            this.btnNextPage = new System.Windows.Forms.Button();
            this.userRowUC1 = new DOPFNsDemo1.UserControls.UserRowUC();
            this.userRowUC2 = new DOPFNsDemo1.UserControls.UserRowUC();
            this.userRowUC3 = new DOPFNsDemo1.UserControls.UserRowUC();
            this.userRowUC4 = new DOPFNsDemo1.UserControls.UserRowUC();
            this.userRowUC5 = new DOPFNsDemo1.UserControls.UserRowUC();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblTableIsActive);
            this.panel1.Controls.Add(this.lblTableFollowers);
            this.panel1.Controls.Add(this.lblTableFollowing);
            this.panel1.Controls.Add(this.lblTableAge);
            this.panel1.Controls.Add(this.lblTableUsername);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(775, 40);
            this.panel1.TabIndex = 7;
            // 
            // lblTableIsActive
            // 
            this.lblTableIsActive.AutoSize = true;
            this.lblTableIsActive.Location = new System.Drawing.Point(606, 12);
            this.lblTableIsActive.Name = "lblTableIsActive";
            this.lblTableIsActive.Size = new System.Drawing.Size(46, 17);
            this.lblTableIsActive.TabIndex = 4;
            this.lblTableIsActive.Text = "Active";
            // 
            // lblTableFollowers
            // 
            this.lblTableFollowers.AutoSize = true;
            this.lblTableFollowers.Location = new System.Drawing.Point(483, 12);
            this.lblTableFollowers.Name = "lblTableFollowers";
            this.lblTableFollowers.Size = new System.Drawing.Size(67, 17);
            this.lblTableFollowers.TabIndex = 3;
            this.lblTableFollowers.Text = "Followers";
            // 
            // lblTableFollowing
            // 
            this.lblTableFollowing.AutoSize = true;
            this.lblTableFollowing.Location = new System.Drawing.Point(342, 12);
            this.lblTableFollowing.Name = "lblTableFollowing";
            this.lblTableFollowing.Size = new System.Drawing.Size(66, 17);
            this.lblTableFollowing.TabIndex = 2;
            this.lblTableFollowing.Text = "Following";
            // 
            // lblTableAge
            // 
            this.lblTableAge.AutoSize = true;
            this.lblTableAge.Location = new System.Drawing.Point(215, 12);
            this.lblTableAge.Name = "lblTableAge";
            this.lblTableAge.Size = new System.Drawing.Size(33, 17);
            this.lblTableAge.TabIndex = 1;
            this.lblTableAge.Text = "Age";
            // 
            // lblTableUsername
            // 
            this.lblTableUsername.AutoSize = true;
            this.lblTableUsername.Location = new System.Drawing.Point(18, 12);
            this.lblTableUsername.Name = "lblTableUsername";
            this.lblTableUsername.Size = new System.Drawing.Size(73, 17);
            this.lblTableUsername.TabIndex = 0;
            this.lblTableUsername.Text = "Username";
            // 
            // btnPrevPage
            // 
            this.btnPrevPage.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.btnPrevPage.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnPrevPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.btnPrevPage.Location = new System.Drawing.Point(609, 435);
            this.btnPrevPage.Name = "btnPrevPage";
            this.btnPrevPage.Size = new System.Drawing.Size(75, 40);
            this.btnPrevPage.TabIndex = 8;
            this.btnPrevPage.Text = "Prev";
            this.btnPrevPage.UseVisualStyleBackColor = false;
            this.btnPrevPage.Click += new System.EventHandler(this.btnPrevPage_Click);
            // 
            // btnNextPage
            // 
            this.btnNextPage.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnNextPage.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnNextPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.btnNextPage.Location = new System.Drawing.Point(690, 435);
            this.btnNextPage.Name = "btnNextPage";
            this.btnNextPage.Size = new System.Drawing.Size(75, 40);
            this.btnNextPage.TabIndex = 9;
            this.btnNextPage.Text = "Next";
            this.btnNextPage.UseVisualStyleBackColor = false;
            this.btnNextPage.Click += new System.EventHandler(this.btnNextPage_Click);
            // 
            // userRowUC1
            // 
            this.userRowUC1.Location = new System.Drawing.Point(0, 32);
            this.userRowUC1.Name = "userRowUC1";
            this.userRowUC1.Size = new System.Drawing.Size(775, 74);
            this.userRowUC1.TabIndex = 10;
            // 
            // userRowUC2
            // 
            this.userRowUC2.Location = new System.Drawing.Point(0, 112);
            this.userRowUC2.Name = "userRowUC2";
            this.userRowUC2.Size = new System.Drawing.Size(775, 74);
            this.userRowUC2.TabIndex = 11;
            // 
            // userRowUC3
            // 
            this.userRowUC3.Location = new System.Drawing.Point(-3, 275);
            this.userRowUC3.Name = "userRowUC3";
            this.userRowUC3.Size = new System.Drawing.Size(775, 74);
            this.userRowUC3.TabIndex = 12;
            // 
            // userRowUC4
            // 
            this.userRowUC4.Location = new System.Drawing.Point(0, 192);
            this.userRowUC4.Name = "userRowUC4";
            this.userRowUC4.Size = new System.Drawing.Size(775, 74);
            this.userRowUC4.TabIndex = 13;
            // 
            // userRowUC5
            // 
            this.userRowUC5.Location = new System.Drawing.Point(0, 355);
            this.userRowUC5.Name = "userRowUC5";
            this.userRowUC5.Size = new System.Drawing.Size(775, 74);
            this.userRowUC5.TabIndex = 14;
            // 
            // UsersTableUC
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.userRowUC5);
            this.Controls.Add(this.userRowUC4);
            this.Controls.Add(this.userRowUC3);
            this.Controls.Add(this.userRowUC2);
            this.Controls.Add(this.userRowUC1);
            this.Controls.Add(this.btnNextPage);
            this.Controls.Add(this.btnPrevPage);
            this.Controls.Add(this.panel1);
            this.Name = "UsersTableUC";
            this.Size = new System.Drawing.Size(775, 478);
            this.Load += new System.EventHandler(this.UsersTableUC_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblTableIsActive;
        private System.Windows.Forms.Label lblTableFollowers;
        private System.Windows.Forms.Label lblTableFollowing;
        private System.Windows.Forms.Label lblTableAge;
        private System.Windows.Forms.Label lblTableUsername;
        private System.Windows.Forms.Button btnPrevPage;
        private System.Windows.Forms.Button btnNextPage;
        private UserRowUC userRowUC1;
        private UserRowUC userRowUC2;
        private UserRowUC userRowUC3;
        private UserRowUC userRowUC4;
        private UserRowUC userRowUC5;
    }
}
