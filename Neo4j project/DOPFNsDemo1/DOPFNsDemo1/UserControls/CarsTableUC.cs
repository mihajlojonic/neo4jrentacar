﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DOPFNsDemo1.UserControls
{
    public partial class CarsTableUC : UserControl
    {
        public CarsTableUC()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void CarsTableUC_Load(object sender, EventArgs e)
        {
            

            Rentacar[] allrentCompanies = DB.Intance().getAllRentacar();
            
            for(int i = 0; i < allrentCompanies.Length; i++)
            {
                this.comboRentCompany.Items.Add(allrentCompanies[i].Ime);
                this.comboRentCompany.Tag = allrentCompanies;
            }

            this.comboRentCompany.SelectedIndex = 1;
        }

        private void comboRentCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            int selectedIndex = this.comboRentCompany.SelectedIndex;

            CarTabela[] carsForSelectedIndex = DB.Intance().getAllCarsFromRent((this.comboRentCompany.Tag as Rentacar[])[selectedIndex].Id);

            for(int i = 0; i < 6; i++)
            {
                if(i < carsForSelectedIndex.Length)
                {
                    if (i == 0)
                    {
                        this.carUC1.Visible = true;
                        this.carUC1.setCarData(carsForSelectedIndex[i]);
                        this.carUC1.setRentcar((this.comboRentCompany.Tag as Rentacar[])[selectedIndex]);
                    }
                    else if (i == 1)
                    {
                        this.carUC2.Visible = true;
                        this.carUC2.setCarData(carsForSelectedIndex[i]);
                        this.carUC2.setRentcar((this.comboRentCompany.Tag as Rentacar[])[selectedIndex]);
                    }
                    else if (i == 2)
                    {
                        this.carUC3.Visible = true;
                        this.carUC3.setCarData(carsForSelectedIndex[i]);
                        this.carUC3.setRentcar((this.comboRentCompany.Tag as Rentacar[])[selectedIndex]);
                    }
                    else if (i == 3)
                    {
                        this.carUC4.Visible = true;
                        this.carUC4.setCarData(carsForSelectedIndex[i]);
                        this.carUC4.setRentcar((this.comboRentCompany.Tag as Rentacar[])[selectedIndex]);
                    }
                    else if (i == 4)
                    {
                        this.carUC5.Visible = true;
                        this.carUC5.setCarData(carsForSelectedIndex[i]);
                        this.carUC5.setRentcar((this.comboRentCompany.Tag as Rentacar[])[selectedIndex]);
                    }
                    else if (i == 5)
                    {
                        this.carUC6.Visible = true;
                        this.carUC6.setCarData(carsForSelectedIndex[i]);
                        this.carUC6.setRentcar((this.comboRentCompany.Tag as Rentacar[])[selectedIndex]);
                    }
                }
                else
                {
                    if (i == 0)
                    {
                        this.carUC1.Visible = false;
                    }
                    else if (i == 1)
                    {
                        this.carUC2.Visible = false;
                    }
                    else if (i == 2)
                    {
                        this.carUC3.Visible = false;
                    }
                    else if (i == 3)
                    {
                        this.carUC4.Visible = false;
                    }
                    else if (i == 4)
                    {
                        this.carUC5.Visible = false;
                    }
                    else if (i == 5)
                    {
                        this.carUC6.Visible = false;
                    }
                }
            }
        }
    }
}
