﻿namespace DOPFNsDemo1.UserControls
{
    partial class LikeUCUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.likeRentacarUC1 = new DOPFNsDemo1.UserControls.LikeRentacarUC();
            this.label1 = new System.Windows.Forms.Label();
            this.likeCompanyUC1 = new DOPFNsDemo1.UserControls.LikeCompanyUC();
            this.SuspendLayout();
            // 
            // likeRentacarUC1
            // 
            this.likeRentacarUC1.BackColor = System.Drawing.Color.White;
            this.likeRentacarUC1.Location = new System.Drawing.Point(66, 241);
            this.likeRentacarUC1.Name = "likeRentacarUC1";
            this.likeRentacarUC1.Size = new System.Drawing.Size(596, 202);
            this.likeRentacarUC1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Modern No. 20", 15.8F);
            this.label1.Location = new System.Drawing.Point(255, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(219, 29);
            this.label1.TabIndex = 2;
            this.label1.Text = "Like something :D";
            // 
            // likeCompanyUC1
            // 
            this.likeCompanyUC1.Location = new System.Drawing.Point(66, 43);
            this.likeCompanyUC1.Name = "likeCompanyUC1";
            this.likeCompanyUC1.Size = new System.Drawing.Size(596, 202);
            this.likeCompanyUC1.TabIndex = 3;
            // 
            // LikeUCUC
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.likeCompanyUC1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.likeRentacarUC1);
            this.Name = "LikeUCUC";
            this.Size = new System.Drawing.Size(775, 478);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private LikeRentacarUC likeRentacarUC1;
        private System.Windows.Forms.Label label1;
        private LikeCompanyUC likeCompanyUC1;
    }
}
