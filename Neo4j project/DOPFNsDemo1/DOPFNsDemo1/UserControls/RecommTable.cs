﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DOPFNsDemo1.UserControls
{
    public partial class RecommTable : UserControl
    {
        public RecommTable()
        {
            InitializeComponent();
        }

        private void RecommTable_Load(object sender, EventArgs e)
        {
            RecommandedRentacar[] recommRent;
            recommRent = DB.Intance().getRecommandedItems(Form1.NewUser.Id);

            this.recommRentUC1.SetData(recommRent[0]);
            this.recommRentUC2.SetData(recommRent[1]);
            this.recommRentUC3.SetData(recommRent[2]);
        }
    }
}
