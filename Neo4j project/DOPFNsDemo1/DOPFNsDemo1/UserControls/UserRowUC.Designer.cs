﻿namespace DOPFNsDemo1.UserControls
{
    partial class UserRowUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblName = new System.Windows.Forms.Label();
            this.lblAge = new System.Windows.Forms.Label();
            this.lblFollowersNum = new System.Windows.Forms.Label();
            this.lblFollowingNum = new System.Windows.Forms.Label();
            this.panelActive = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 17.8F);
            this.lblName.Location = new System.Drawing.Point(3, 19);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(150, 36);
            this.lblName.TabIndex = 0;
            this.lblName.Text = "Username";
            // 
            // lblAge
            // 
            this.lblAge.AutoSize = true;
            this.lblAge.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.8F);
            this.lblAge.Location = new System.Drawing.Point(210, 16);
            this.lblAge.Name = "lblAge";
            this.lblAge.Size = new System.Drawing.Size(55, 39);
            this.lblAge.TabIndex = 1;
            this.lblAge.Text = "25";
            // 
            // lblFollowersNum
            // 
            this.lblFollowersNum.AutoSize = true;
            this.lblFollowersNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.8F);
            this.lblFollowersNum.Location = new System.Drawing.Point(323, 5);
            this.lblFollowersNum.Name = "lblFollowersNum";
            this.lblFollowersNum.Size = new System.Drawing.Size(101, 54);
            this.lblFollowersNum.TabIndex = 2;
            this.lblFollowersNum.Text = "505";
            // 
            // lblFollowingNum
            // 
            this.lblFollowingNum.AutoSize = true;
            this.lblFollowingNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.8F);
            this.lblFollowingNum.Location = new System.Drawing.Point(464, 5);
            this.lblFollowingNum.Name = "lblFollowingNum";
            this.lblFollowingNum.Size = new System.Drawing.Size(101, 54);
            this.lblFollowingNum.TabIndex = 3;
            this.lblFollowingNum.Text = "200";
            // 
            // panelActive
            // 
            this.panelActive.BackColor = System.Drawing.Color.Red;
            this.panelActive.Location = new System.Drawing.Point(604, 28);
            this.panelActive.Name = "panelActive";
            this.panelActive.Size = new System.Drawing.Size(20, 17);
            this.panelActive.TabIndex = 4;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Blue;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.8F);
            this.button1.ForeColor = System.Drawing.SystemColors.Control;
            this.button1.Location = new System.Drawing.Point(666, 15);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(106, 40);
            this.button1.TabIndex = 5;
            this.button1.Text = "Follow";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // UserRowUC
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panelActive);
            this.Controls.Add(this.lblFollowingNum);
            this.Controls.Add(this.lblFollowersNum);
            this.Controls.Add(this.lblAge);
            this.Controls.Add(this.lblName);
            this.Name = "UserRowUC";
            this.Size = new System.Drawing.Size(775, 74);
            this.Load += new System.EventHandler(this.UserRowUC_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblAge;
        private System.Windows.Forms.Label lblFollowersNum;
        private System.Windows.Forms.Label lblFollowingNum;
        private System.Windows.Forms.Panel panelActive;
        private System.Windows.Forms.Button button1;
    }
}
