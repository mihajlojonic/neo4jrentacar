﻿namespace DOPFNsDemo1.UserControls
{
    partial class LikeRentacarUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblRentacar1 = new System.Windows.Forms.Label();
            this.lblRentacar2 = new System.Windows.Forms.Label();
            this.lblRentacar3 = new System.Windows.Forms.Label();
            this.lblRentacar4 = new System.Windows.Forms.Label();
            this.lblRentacar5 = new System.Windows.Forms.Label();
            this.lblRentacar6 = new System.Windows.Forms.Label();
            this.btnRent1 = new System.Windows.Forms.Button();
            this.btnRent2 = new System.Windows.Forms.Button();
            this.btnRent3 = new System.Windows.Forms.Button();
            this.btnRent4 = new System.Windows.Forms.Button();
            this.btnRent5 = new System.Windows.Forms.Button();
            this.btnRent6 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblRentacar1
            // 
            this.lblRentacar1.AutoSize = true;
            this.lblRentacar1.Font = new System.Drawing.Font("Broadway", 16.2F);
            this.lblRentacar1.ForeColor = System.Drawing.Color.DimGray;
            this.lblRentacar1.Location = new System.Drawing.Point(18, 21);
            this.lblRentacar1.Name = "lblRentacar1";
            this.lblRentacar1.Size = new System.Drawing.Size(175, 32);
            this.lblRentacar1.TabIndex = 12;
            this.lblRentacar1.Text = "Rentacar1";
            // 
            // lblRentacar2
            // 
            this.lblRentacar2.AutoSize = true;
            this.lblRentacar2.Font = new System.Drawing.Font("Broadway", 16.2F);
            this.lblRentacar2.ForeColor = System.Drawing.Color.DimGray;
            this.lblRentacar2.Location = new System.Drawing.Point(18, 85);
            this.lblRentacar2.Name = "lblRentacar2";
            this.lblRentacar2.Size = new System.Drawing.Size(175, 32);
            this.lblRentacar2.TabIndex = 13;
            this.lblRentacar2.Text = "Rentacar2";
            // 
            // lblRentacar3
            // 
            this.lblRentacar3.AutoSize = true;
            this.lblRentacar3.Font = new System.Drawing.Font("Broadway", 16.2F);
            this.lblRentacar3.ForeColor = System.Drawing.Color.DimGray;
            this.lblRentacar3.Location = new System.Drawing.Point(18, 142);
            this.lblRentacar3.Name = "lblRentacar3";
            this.lblRentacar3.Size = new System.Drawing.Size(175, 32);
            this.lblRentacar3.TabIndex = 14;
            this.lblRentacar3.Text = "Rentacar3";
            // 
            // lblRentacar4
            // 
            this.lblRentacar4.AutoSize = true;
            this.lblRentacar4.Font = new System.Drawing.Font("Broadway", 16.2F);
            this.lblRentacar4.ForeColor = System.Drawing.Color.DimGray;
            this.lblRentacar4.Location = new System.Drawing.Point(316, 21);
            this.lblRentacar4.Name = "lblRentacar4";
            this.lblRentacar4.Size = new System.Drawing.Size(175, 32);
            this.lblRentacar4.TabIndex = 15;
            this.lblRentacar4.Text = "Rentacar4";
            // 
            // lblRentacar5
            // 
            this.lblRentacar5.AutoSize = true;
            this.lblRentacar5.Font = new System.Drawing.Font("Broadway", 16.2F);
            this.lblRentacar5.ForeColor = System.Drawing.Color.DimGray;
            this.lblRentacar5.Location = new System.Drawing.Point(316, 85);
            this.lblRentacar5.Name = "lblRentacar5";
            this.lblRentacar5.Size = new System.Drawing.Size(175, 32);
            this.lblRentacar5.TabIndex = 16;
            this.lblRentacar5.Text = "Rentacar5";
            // 
            // lblRentacar6
            // 
            this.lblRentacar6.AutoSize = true;
            this.lblRentacar6.Font = new System.Drawing.Font("Broadway", 16.2F);
            this.lblRentacar6.ForeColor = System.Drawing.Color.DimGray;
            this.lblRentacar6.Location = new System.Drawing.Point(316, 142);
            this.lblRentacar6.Name = "lblRentacar6";
            this.lblRentacar6.Size = new System.Drawing.Size(175, 32);
            this.lblRentacar6.TabIndex = 17;
            this.lblRentacar6.Text = "Rentacar6";
            // 
            // btnRent1
            // 
            this.btnRent1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(0)))), ((int)(((byte)(127)))));
            this.btnRent1.ForeColor = System.Drawing.Color.White;
            this.btnRent1.Location = new System.Drawing.Point(215, 21);
            this.btnRent1.Name = "btnRent1";
            this.btnRent1.Size = new System.Drawing.Size(75, 32);
            this.btnRent1.TabIndex = 18;
            this.btnRent1.Text = "Like";
            this.btnRent1.UseVisualStyleBackColor = false;
            this.btnRent1.Click += new System.EventHandler(this.btnRent1_Click);
            // 
            // btnRent2
            // 
            this.btnRent2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(0)))), ((int)(((byte)(127)))));
            this.btnRent2.ForeColor = System.Drawing.Color.White;
            this.btnRent2.Location = new System.Drawing.Point(215, 85);
            this.btnRent2.Name = "btnRent2";
            this.btnRent2.Size = new System.Drawing.Size(75, 32);
            this.btnRent2.TabIndex = 19;
            this.btnRent2.Text = "Like";
            this.btnRent2.UseVisualStyleBackColor = false;
            this.btnRent2.Click += new System.EventHandler(this.btnRent2_Click);
            // 
            // btnRent3
            // 
            this.btnRent3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(0)))), ((int)(((byte)(127)))));
            this.btnRent3.ForeColor = System.Drawing.Color.White;
            this.btnRent3.Location = new System.Drawing.Point(215, 142);
            this.btnRent3.Name = "btnRent3";
            this.btnRent3.Size = new System.Drawing.Size(75, 32);
            this.btnRent3.TabIndex = 20;
            this.btnRent3.Text = "Like";
            this.btnRent3.UseVisualStyleBackColor = false;
            this.btnRent3.Click += new System.EventHandler(this.btnRent3_Click);
            // 
            // btnRent4
            // 
            this.btnRent4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(0)))), ((int)(((byte)(127)))));
            this.btnRent4.ForeColor = System.Drawing.Color.White;
            this.btnRent4.Location = new System.Drawing.Point(507, 21);
            this.btnRent4.Name = "btnRent4";
            this.btnRent4.Size = new System.Drawing.Size(75, 32);
            this.btnRent4.TabIndex = 21;
            this.btnRent4.Text = "Like";
            this.btnRent4.UseVisualStyleBackColor = false;
            this.btnRent4.Click += new System.EventHandler(this.btnRent4_Click);
            // 
            // btnRent5
            // 
            this.btnRent5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(0)))), ((int)(((byte)(127)))));
            this.btnRent5.ForeColor = System.Drawing.Color.White;
            this.btnRent5.Location = new System.Drawing.Point(507, 85);
            this.btnRent5.Name = "btnRent5";
            this.btnRent5.Size = new System.Drawing.Size(75, 32);
            this.btnRent5.TabIndex = 22;
            this.btnRent5.Text = "Like";
            this.btnRent5.UseVisualStyleBackColor = false;
            this.btnRent5.Click += new System.EventHandler(this.btnRent5_Click);
            // 
            // btnRent6
            // 
            this.btnRent6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(0)))), ((int)(((byte)(127)))));
            this.btnRent6.ForeColor = System.Drawing.Color.White;
            this.btnRent6.Location = new System.Drawing.Point(507, 142);
            this.btnRent6.Name = "btnRent6";
            this.btnRent6.Size = new System.Drawing.Size(75, 32);
            this.btnRent6.TabIndex = 23;
            this.btnRent6.Text = "Like";
            this.btnRent6.UseVisualStyleBackColor = false;
            this.btnRent6.Click += new System.EventHandler(this.btnRent6_Click);
            // 
            // LikeRentacarUC
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.btnRent6);
            this.Controls.Add(this.btnRent5);
            this.Controls.Add(this.btnRent4);
            this.Controls.Add(this.btnRent3);
            this.Controls.Add(this.btnRent2);
            this.Controls.Add(this.btnRent1);
            this.Controls.Add(this.lblRentacar6);
            this.Controls.Add(this.lblRentacar5);
            this.Controls.Add(this.lblRentacar4);
            this.Controls.Add(this.lblRentacar3);
            this.Controls.Add(this.lblRentacar2);
            this.Controls.Add(this.lblRentacar1);
            this.Name = "LikeRentacarUC";
            this.Size = new System.Drawing.Size(596, 202);
            this.Load += new System.EventHandler(this.LikeRentacarUC_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblRentacar1;
        private System.Windows.Forms.Label lblRentacar2;
        private System.Windows.Forms.Label lblRentacar3;
        private System.Windows.Forms.Label lblRentacar4;
        private System.Windows.Forms.Label lblRentacar5;
        private System.Windows.Forms.Label lblRentacar6;
        private System.Windows.Forms.Button btnRent1;
        private System.Windows.Forms.Button btnRent2;
        private System.Windows.Forms.Button btnRent3;
        private System.Windows.Forms.Button btnRent4;
        private System.Windows.Forms.Button btnRent5;
        private System.Windows.Forms.Button btnRent6;
    }
}
