﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DOPFNsDemo1.UserControls
{
    public partial class UsersTableUC : UserControl
    {
        int page = 0;
        UserRowUC[] rowsUsers = new UserRowUC[5];


        public UsersTableUC()
        {
            InitializeComponent();
            rowsUsers[0] = new UserRowUC();
            rowsUsers[0] = this.userRowUC1;
            rowsUsers[1] = new UserRowUC();
            rowsUsers[1] = this.userRowUC2;
            rowsUsers[2] = new UserRowUC();
            rowsUsers[2] = this.userRowUC3;
            rowsUsers[3] = new UserRowUC();
            rowsUsers[3] = this.userRowUC4;
            rowsUsers[4] = new UserRowUC();
            rowsUsers[4] = this.userRowUC5;
        }

        public void SetUsers(KorisnikTabela[] users)
        {
            int rowNum = 0;
            for(int i = page * 5; i < page * 5 + 5; i++)
            {
                if(i < users.Length)
                {   
                    if (rowNum == 0)
                    {
                        this.userRowUC1.Visible = true;
                        this.userRowUC1.SetUsers(users[i]);
                        rowNum++;
                    }
                    else if (rowNum == 1)
                    {
                        this.userRowUC2.Visible = true;
                        this.userRowUC2.SetUsers(users[i]);
                        rowNum++;
                    }
                    else if (rowNum == 2)
                    {
                        this.userRowUC3.Visible = true;
                        this.userRowUC3.SetUsers(users[i]);
                        rowNum++;
                    }
                    else if (rowNum == 3)
                    {
                        this.userRowUC4.Visible = true;
                        this.userRowUC4.SetUsers(users[i]);
                        rowNum++;
                    }
                    else if (rowNum == 4)
                    {
                        this.userRowUC5.Visible = true;
                        this.userRowUC5.SetUsers(users[i]);
                        rowNum++;
                    }
                }
                else
                {
                    if (rowNum == 0)
                    {
                        this.userRowUC1.Visible = false;
                        rowNum++;
                    }
                    else if (rowNum == 1)
                    {
                        this.userRowUC2.Visible = false;
                        rowNum++;
                    }
                    else if (rowNum == 2)
                    {
                        this.userRowUC3.Visible = false;
                        rowNum++;
                    }
                    else if (rowNum == 3)
                    {
                        this.userRowUC4.Visible = false;
                        rowNum++;
                    }
                    else if (rowNum == 4)
                    {
                        this.userRowUC5.Visible = false;
                        rowNum++;
                    }
                }
            }
        }

        public void updateUsers(KorisnikTabela[] users)
        {

        }

        private void btnPrevPage_Click(object sender, EventArgs e)
        {
            if(page > 0)
            {
                page--;
                KorisnikTabela[] allUsers = DB.Intance().getUsersForTable(Form1.NewUser.Id);
                SetUsers(allUsers);
            }
        }

        private void btnNextPage_Click(object sender, EventArgs e)
        {
            KorisnikTabela[] allUsers = DB.Intance().getUsersForTable(Form1.NewUser.Id);
            if((page ) <= allUsers.Count() / 5)
            {
                page++;
                SetUsers(allUsers);
            }
        }

        private void UsersTableUC_Load(object sender, EventArgs e)
        {

        }
    }
}
