﻿namespace DOPFNsDemo1.UserControls
{
    partial class RecommRentUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblImeRentacara = new System.Windows.Forms.Label();
            this.lblDrzava = new System.Windows.Forms.Label();
            this.lblNumOfFriends = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblImeRentacara
            // 
            this.lblImeRentacara.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.lblImeRentacara.ForeColor = System.Drawing.Color.White;
            this.lblImeRentacara.Location = new System.Drawing.Point(0, 0);
            this.lblImeRentacara.Name = "lblImeRentacara";
            this.lblImeRentacara.Size = new System.Drawing.Size(150, 24);
            this.lblImeRentacara.TabIndex = 0;
            this.lblImeRentacara.Text = "HIROSIMArent";
            this.lblImeRentacara.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDrzava
            // 
            this.lblDrzava.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblDrzava.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblDrzava.ForeColor = System.Drawing.SystemColors.Control;
            this.lblDrzava.Location = new System.Drawing.Point(0, 22);
            this.lblDrzava.Name = "lblDrzava";
            this.lblDrzava.Size = new System.Drawing.Size(147, 29);
            this.lblDrzava.TabIndex = 1;
            this.lblDrzava.Text = "Japan";
            this.lblDrzava.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblDrzava.Click += new System.EventHandler(this.lblDrzava_Click);
            // 
            // lblNumOfFriends
            // 
            this.lblNumOfFriends.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.8F);
            this.lblNumOfFriends.Location = new System.Drawing.Point(3, 77);
            this.lblNumOfFriends.Name = "lblNumOfFriends";
            this.lblNumOfFriends.Size = new System.Drawing.Size(144, 48);
            this.lblNumOfFriends.TabIndex = 2;
            this.lblNumOfFriends.Text = "36 friends like this company";
            this.lblNumOfFriends.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // RecommRentUC
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.ForestGreen;
            this.Controls.Add(this.lblNumOfFriends);
            this.Controls.Add(this.lblDrzava);
            this.Controls.Add(this.lblImeRentacara);
            this.Name = "RecommRentUC";
            this.Load += new System.EventHandler(this.RecommRentUC_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblImeRentacara;
        private System.Windows.Forms.Label lblDrzava;
        private System.Windows.Forms.Label lblNumOfFriends;
    }
}
