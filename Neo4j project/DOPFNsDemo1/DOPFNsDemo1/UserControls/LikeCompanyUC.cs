﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DOPFNsDemo1.UserControls
{
    public partial class LikeCompanyUC : UserControl
    {
        DB dbInstance;
        CompanyLiking[] companies;
        int clicked = 0;

        public LikeCompanyUC()
        {
            InitializeComponent();
            dbInstance = DB.Intance();
            companies = dbInstance.getAllCompaniesWithLiking(Form1.NewUser.Id);
        }

        private void LikeCompanyUC_Load(object sender, EventArgs e)
        {
            
            this.lblCompany1.Text = companies[0].Comp.CompanyName;

            if (companies[0].IsLiked == true)
                btnLike1.Enabled = false;
            else
                btnLike1.Enabled = true;

            this.lblCompany2.Text = companies[1].Comp.CompanyName;

            if (companies[1].IsLiked == true)
                btnLike2.Enabled = false;
            else
                btnLike2.Enabled = true;

            this.lblCompany3.Text = companies[2].Comp.CompanyName;

            if (companies[2].IsLiked == true)
                btnLike3.Enabled = false;
            else
                btnLike3.Enabled = true;

            this.lblCompany4.Text = companies[3].Comp.CompanyName;

            if (companies[3].IsLiked == true)
                btnLike4.Enabled = false;
            else
                btnLike4.Enabled = true;

            this.lblCompany5.Text = companies[4].Comp.CompanyName;

            if (companies[4].IsLiked == true)
                btnLike5.Enabled = false;
            else
                btnLike5.Enabled = true;

            this.lblCompany6.Text = companies[5].Comp.CompanyName;

            if (companies[5].IsLiked == true)
                btnLike6.Enabled = false;
            else
                btnLike6.Enabled = true;
        }

        private void clickFunction()
        {
            string id = Form1.NewUser.Id;
            dbInstance.createRelationshipJeLjubitelj(id, companies[clicked].Comp.CompanyName);
        }

        private void btnLike1_Click_1(object sender, EventArgs e)
        {
            clicked = 0;
            clickFunction();
            btnLike1.Enabled = false;
        }

        private void btnLike2_Click_1(object sender, EventArgs e)
        {
            clicked = 1;
            clickFunction();
            btnLike2.Enabled = false;
        }

        private void btnLike3_Click_1(object sender, EventArgs e)
        {
            clicked = 2;
            clickFunction();
            btnLike3.Enabled = false;
        }

        private void btnLike4_Click_1(object sender, EventArgs e)
        {
            clicked = 3;
            clickFunction();
            btnLike4.Enabled = false;
        }

        private void btnLike5_Click_1(object sender, EventArgs e)
        {
            clicked = 4;
            clickFunction();
            btnLike5.Enabled = false;
        }

        private void btnLike6_Click_1(object sender, EventArgs e)
        {
            clicked = 5;
            clickFunction();
            btnLike6.Enabled = false;
        }
    }
}
