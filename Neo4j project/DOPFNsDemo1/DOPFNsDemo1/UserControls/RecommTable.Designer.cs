﻿namespace DOPFNsDemo1.UserControls
{
    partial class RecommTable
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.recommRentUC1 = new DOPFNsDemo1.UserControls.RecommRentUC();
            this.recommRentUC2 = new DOPFNsDemo1.UserControls.RecommRentUC();
            this.recommRentUC3 = new DOPFNsDemo1.UserControls.RecommRentUC();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.panel1.Controls.Add(this.recommRentUC3);
            this.panel1.Controls.Add(this.recommRentUC2);
            this.panel1.Controls.Add(this.recommRentUC1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(30, 60);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(710, 353);
            this.panel1.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Modern No. 20", 19.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(710, 59);
            this.label1.TabIndex = 3;
            this.label1.Text = "Recommanded companies...";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // recommRentUC1
            // 
            this.recommRentUC1.BackColor = System.Drawing.Color.ForestGreen;
            this.recommRentUC1.Location = new System.Drawing.Point(49, 99);
            this.recommRentUC1.Name = "recommRentUC1";
            this.recommRentUC1.Size = new System.Drawing.Size(150, 150);
            this.recommRentUC1.TabIndex = 4;
            // 
            // recommRentUC2
            // 
            this.recommRentUC2.BackColor = System.Drawing.Color.ForestGreen;
            this.recommRentUC2.Location = new System.Drawing.Point(262, 101);
            this.recommRentUC2.Name = "recommRentUC2";
            this.recommRentUC2.Size = new System.Drawing.Size(150, 150);
            this.recommRentUC2.TabIndex = 5;
            // 
            // recommRentUC3
            // 
            this.recommRentUC3.BackColor = System.Drawing.Color.ForestGreen;
            this.recommRentUC3.Location = new System.Drawing.Point(496, 101);
            this.recommRentUC3.Name = "recommRentUC3";
            this.recommRentUC3.Size = new System.Drawing.Size(150, 150);
            this.recommRentUC3.TabIndex = 6;
            // 
            // RecommTable
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.ForestGreen;
            this.Controls.Add(this.panel1);
            this.Name = "RecommTable";
            this.Size = new System.Drawing.Size(775, 478);
            this.Load += new System.EventHandler(this.RecommTable_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private RecommRentUC recommRentUC3;
        private RecommRentUC recommRentUC2;
        private RecommRentUC recommRentUC1;
    }
}
