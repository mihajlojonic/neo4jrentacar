﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DOPFNsDemo1.UserControls
{
    public partial class LikeRentacarUC : UserControl
    {
        DB dbInstance;
        RentacarLiking[] rentacars;
        int clicked = 0;

        public LikeRentacarUC()
        {
            InitializeComponent();
            dbInstance = DB.Intance();
            rentacars = dbInstance.getAllRentacarsWithLiking(Form1.NewUser.Id);
        }

        private void clickFunction()
        {
            string id = Form1.NewUser.Id;
            dbInstance.createRelationshipJeKlijent(id, rentacars[clicked].Rent.Ime);
        }

        private void LikeRentacarUC_Load(object sender, EventArgs e)
        {
            this.lblRentacar1.Text = rentacars[0].Rent.Ime;
            if (rentacars[0].IsLiked == true)
                btnRent1.Enabled = false;
            else
                btnRent1.Enabled = true;

            this.lblRentacar2.Text = rentacars[1].Rent.Ime;
            if (rentacars[1].IsLiked == true)
                btnRent2.Enabled = false;
            else
                btnRent2.Enabled = true;

            this.lblRentacar3.Text = rentacars[2].Rent.Ime;
            if (rentacars[2].IsLiked == true)
                btnRent3.Enabled = false;
            else
                btnRent3.Enabled = true;

            this.lblRentacar4.Text = rentacars[3].Rent.Ime;
            if (rentacars[3].IsLiked == true)
                btnRent4.Enabled = false;
            else
                btnRent4.Enabled = true;

            this.lblRentacar5.Text = rentacars[4].Rent.Ime;
            if (rentacars[4].IsLiked == true)
                btnRent5.Enabled = false;
            else
                btnRent5.Enabled = true;

            this.lblRentacar6.Text = rentacars[5].Rent.Ime;
            if (rentacars[5].IsLiked == true)
                btnRent6.Enabled = false;
            else
                btnRent6.Enabled = true;
        }


        //events

        private void btnRent1_Click(object sender, EventArgs e)
        {
            clicked = 0;
            clickFunction();
            btnRent1.Enabled = false;
        }

        private void btnRent2_Click(object sender, EventArgs e)
        {
            clicked = 1;
            clickFunction();
            btnRent2.Enabled = false;
        }

        private void btnRent3_Click(object sender, EventArgs e)
        {
            clicked = 2;
            clickFunction();
            btnRent3.Enabled = false;
        }

        private void btnRent4_Click(object sender, EventArgs e)
        {
            clicked = 3;
            clickFunction();
            btnRent4.Enabled = false;
        }

        private void btnRent5_Click(object sender, EventArgs e)
        {
            clicked = 4;
            clickFunction();
            btnRent5.Enabled = false;
        }

        private void btnRent6_Click(object sender, EventArgs e)
        {
            clicked = 5;
            clickFunction();
            btnRent6.Enabled = false;
        }
    }
}
