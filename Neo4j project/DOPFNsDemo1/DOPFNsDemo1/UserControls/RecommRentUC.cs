﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DOPFNsDemo1.UserControls
{
    public partial class RecommRentUC : UserControl
    {
        public RecommRentUC()
        {
            InitializeComponent();
        }

        private void lblDrzava_Click(object sender, EventArgs e)
        {

        }

        private void RecommRentUC_Load(object sender, EventArgs e)
        {

        }

        public void SetData(RecommandedRentacar rentTable)
        {
            this.lblImeRentacara.Text = rentTable.Rent.Ime;
            this.lblDrzava.Text = rentTable.Rent.Drzava;
            this.lblNumOfFriends.Text = rentTable.NumberOfFriends + " of your friends like our company";
        }
    }
}
