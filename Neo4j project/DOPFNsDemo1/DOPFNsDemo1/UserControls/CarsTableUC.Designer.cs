﻿namespace DOPFNsDemo1.UserControls
{
    partial class CarsTableUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.carUC1 = new DOPFNsDemo1.UserControls.CarUC();
            this.carUC2 = new DOPFNsDemo1.UserControls.CarUC();
            this.carUC3 = new DOPFNsDemo1.UserControls.CarUC();
            this.carUC4 = new DOPFNsDemo1.UserControls.CarUC();
            this.carUC5 = new DOPFNsDemo1.UserControls.CarUC();
            this.carUC6 = new DOPFNsDemo1.UserControls.CarUC();
            this.comboRentCompany = new System.Windows.Forms.ComboBox();
            this.lblrentacar = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // carUC1
            // 
            this.carUC1.Location = new System.Drawing.Point(25, 52);
            this.carUC1.Name = "carUC1";
            this.carUC1.Size = new System.Drawing.Size(189, 209);
            this.carUC1.TabIndex = 0;
            // 
            // carUC2
            // 
            this.carUC2.Location = new System.Drawing.Point(267, 52);
            this.carUC2.Name = "carUC2";
            this.carUC2.Size = new System.Drawing.Size(189, 209);
            this.carUC2.TabIndex = 1;
            // 
            // carUC3
            // 
            this.carUC3.Location = new System.Drawing.Point(504, 52);
            this.carUC3.Name = "carUC3";
            this.carUC3.Size = new System.Drawing.Size(189, 209);
            this.carUC3.TabIndex = 2;
            // 
            // carUC4
            // 
            this.carUC4.Location = new System.Drawing.Point(25, 266);
            this.carUC4.Name = "carUC4";
            this.carUC4.Size = new System.Drawing.Size(189, 209);
            this.carUC4.TabIndex = 3;
            // 
            // carUC5
            // 
            this.carUC5.Location = new System.Drawing.Point(267, 266);
            this.carUC5.Name = "carUC5";
            this.carUC5.Size = new System.Drawing.Size(189, 209);
            this.carUC5.TabIndex = 4;
            // 
            // carUC6
            // 
            this.carUC6.Location = new System.Drawing.Point(504, 266);
            this.carUC6.Name = "carUC6";
            this.carUC6.Size = new System.Drawing.Size(189, 209);
            this.carUC6.TabIndex = 5;
            // 
            // comboRentCompany
            // 
            this.comboRentCompany.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboRentCompany.FormattingEnabled = true;
            this.comboRentCompany.Location = new System.Drawing.Point(226, 25);
            this.comboRentCompany.Name = "comboRentCompany";
            this.comboRentCompany.Size = new System.Drawing.Size(312, 24);
            this.comboRentCompany.TabIndex = 6;
            this.comboRentCompany.SelectedIndexChanged += new System.EventHandler(this.comboRentCompany_SelectedIndexChanged);
            // 
            // lblrentacar
            // 
            this.lblrentacar.AutoSize = true;
            this.lblrentacar.Font = new System.Drawing.Font("Modern No. 20", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblrentacar.Location = new System.Drawing.Point(295, 1);
            this.lblrentacar.Name = "lblrentacar";
            this.lblrentacar.Size = new System.Drawing.Size(186, 21);
            this.lblrentacar.TabIndex = 7;
            this.lblrentacar.Text = "Rent a car company:";
            this.lblrentacar.Click += new System.EventHandler(this.label1_Click);
            // 
            // CarsTableUC
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.lblrentacar);
            this.Controls.Add(this.comboRentCompany);
            this.Controls.Add(this.carUC6);
            this.Controls.Add(this.carUC5);
            this.Controls.Add(this.carUC4);
            this.Controls.Add(this.carUC3);
            this.Controls.Add(this.carUC2);
            this.Controls.Add(this.carUC1);
            this.Name = "CarsTableUC";
            this.Size = new System.Drawing.Size(775, 478);
            this.Load += new System.EventHandler(this.CarsTableUC_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CarUC carUC1;
        private CarUC carUC2;
        private CarUC carUC3;
        private CarUC carUC4;
        private CarUC carUC5;
        private CarUC carUC6;
        private System.Windows.Forms.ComboBox comboRentCompany;
        private System.Windows.Forms.Label lblrentacar;
    }
}
