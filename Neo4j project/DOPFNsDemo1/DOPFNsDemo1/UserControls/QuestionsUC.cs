﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DOPFNsDemo1.UserControls
{
    public partial class QuestionsUC : UserControl
    {
        DB dbInstance;
        Company[] companies;
        int clicked = 0;

        public QuestionsUC()
        {
            InitializeComponent();
            dbInstance = DB.Intance();
            companies = dbInstance.getAllCompanies();
        }

        private void QuestionsUC_Load(object sender, EventArgs e)
        {
            this.lblCompany1.Text = companies[0].CompanyName;
            this.lblCompany2.Text = companies[1].CompanyName;
            this.lblCompany3.Text = companies[2].CompanyName;
            this.lblCompany4.Text = companies[3].CompanyName;
            this.lblCompany5.Text = companies[4].CompanyName;
            this.lblCompany6.Text = companies[5].CompanyName;
        }

        private void clickFunction()
        {
            string id = Form1.NewUser.Id;

            dbInstance.createRelationshipJeLjubitelj(id, companies[clicked].CompanyName);
            Forms.formDashboard obj = new Forms.formDashboard(Form1.NewUser);
            obj.Show();
            this.Hide();
        }

        #region Events
        private void btnLike1_Click(object sender, EventArgs e)
        {
            clicked = 0;
            clickFunction();
        }

        private void btnLike2_Click(object sender, EventArgs e)
        {
            clicked = 1;
            clickFunction();
        }

        private void btnLike3_Click(object sender, EventArgs e)
        {
            clicked = 2;
            clickFunction();
        }

        private void btnLike4_Click(object sender, EventArgs e)
        {
            clicked = 3;
            clickFunction();
        }

        private void btnLike5_Click(object sender, EventArgs e)
        {
            clicked = 4;
            clickFunction();
        }

        private void btnLike6_Click(object sender, EventArgs e)
        {
            clicked = 5;
            clickFunction();
        }
        #endregion
    }
}
