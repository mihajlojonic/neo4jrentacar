﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOPFNsDemo1
{
    class RC4
    {
        private string encKey = "";
        private string encKeyAscii = "";
        private long keyLen = 255;
        private byte[] key = new byte[255];

        public RC4()
        {
            this.EncKey = "Neo4j";
        }

        public string EncKey
        {
            get
            {
                return this.encKey;
            }
            set
            {
                if (this.encKey != value)
                {
                    this.encKey = value;
                    long index2 = 0;
                    Encoding ascii = Encoding.ASCII;
                    Encoding unicode = Encoding.Unicode;

                    byte[] asciiBytes = Encoding.Convert(unicode, ascii, unicode.GetBytes(this.encKey));
                    char[] asciiChars = new char[ascii.GetCharCount(asciiBytes, 0, asciiBytes.Length)];
                    ascii.GetChars(asciiBytes, 0, asciiBytes.Length, asciiChars, 0);
                    this.encKeyAscii = new string(asciiChars);
                    long KeyLen = encKey.Length;
                    for (long count = 0; count < keyLen; count++)
                    {
                        this.key[count] = (byte)count;
                    }
                    for (long count = 0; count < keyLen; count++)
                    {
                        index2 = (index2 + key[count] + asciiChars[count % KeyLen]) % keyLen;

                        byte temp = key[count];
                        key[count] = key[index2];
                        key[index2] = temp;
                    }
                }
            }
        }

        public string Crypt(string plainText)
        {
            long i = 0;
            long j = 0;
            Encoding encDef = Encoding.Default;
            byte[] input = encDef.GetBytes(plainText);
            byte[] output = new byte[input.Length];
            byte[] localKey = new byte[keyLen];
            key.CopyTo(localKey, 0);
            long ChipherLen = input.Length + 1;

            for (long offset = 0; offset < input.Length; offset++)
            {
                i = (i + 1) % keyLen;
                j = (j + localKey[i]) % keyLen;
                byte temp = localKey[i];
                localKey[i] = localKey[j];
                localKey[j] = temp;
                byte a = input[offset];
                byte b = localKey[(localKey[i] + localKey[j]) % keyLen];
                output[offset] = (byte)((int)a ^ (int)b);
            }

            char[] outArrChar = new char[encDef.GetCharCount(output, 0, output.Length)];
            encDef.GetChars(output, 0, output.Length, outArrChar, 0);
            return new string(outArrChar);

        }

        //decrypt text
        public string Decrypt(string plainText)
        {
            return Crypt(plainText);
        }
    }
}
