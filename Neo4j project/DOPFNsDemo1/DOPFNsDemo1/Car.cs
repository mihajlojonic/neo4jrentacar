﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOPFNsDemo1
{
    public class Car
    {
        public string Model { get; set; }
        public string Year { get; set; }
        public string BrVrata { get; set; }
        public string Potrosnja { get; set; }
        public string Snaga { get; set; }
        public string Id { get; set; }
    }
}
