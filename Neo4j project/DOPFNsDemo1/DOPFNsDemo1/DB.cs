﻿using Neo4jClient;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOPFNsDemo1
{
    class DB
    {
        private static DB dbInstance;
        GraphClient client = new GraphClient(new Uri("http://localhost:11003/db/data"));
        RC4 rc4 = new RC4();
        protected DB()
        {
            client.Connect();
        }

        public static DB Intance()
        {
            if (dbInstance == null)
            {
                dbInstance = new DB();
            }

            return dbInstance;
        }

        public Korisnik[] getAllUsers()
        {
            var query = client.Cypher
                .Match("(k:Korisnik)")
                .Return<Korisnik>("k");

            var results = query.Results;
            Korisnik[] kor = results.ToArray<Korisnik>();

            return kor;
        }

        public Rentacar[] getAllRentacar()
        {
            var queryRentacar = client.Cypher
                .Match("(rent:Rentacar)")
                .Return<Rentacar>("rent");

            var resultsRentacar = queryRentacar.Results;
            Rentacar[] rent = resultsRentacar.ToArray<Rentacar>();

            return rent;
        }

        public CarTabela[] getAllCarsFromRent(string idOfRent)
        {
            var q = client.Cypher
                .Match("(rent1:Rentacar)-[pos1]->(c1:Car)<-[:PROIZVODI]-(comp1:Company)")
                .Where((Rentacar rent1) => rent1.Id == idOfRent)
                .Return((c1, comp1, pos1) => new
                {
                    CollectionCars = c1.As<Car>(),
                    CollectionComp = comp1.As<Company>(),
                    CollectionPos = pos1.As<VezaPoseduje>()
                });

            var ret = q.Results;
            CarTabela[] returnArray = new CarTabela[ret.Count()];
            for (int count = 0; count < ret.Count(); count++)
                returnArray[count] = new CarTabela() { Car = new Car() };

            for (int i = 0; i < returnArray.Length; i++)
            {
                returnArray[i].Car = ret.ElementAt(i).CollectionCars;
                returnArray[i].Cena = ret.ElementAt(i).CollectionPos.Cena;
                returnArray[i].Marka = ret.ElementAt(i).CollectionComp.CompanyName;
            }

            return returnArray;
        }

        public Company[] getAllCompanies()
        {
            var queryKompanije = client.Cypher
                .Match("(komp:Company)")
                .Return<Company>("komp");

            var resultsCompany = queryKompanije.Results;
            Company[] komp = resultsCompany.ToArray<Company>();

            return komp;
        }

        public bool checkUser(string name, string password)
        {
            password = rc4.Crypt(password);
            var query = client.Cypher
               .Match("(k:Korisnik)")
               .Where((Korisnik k) => k.Name == name)
               .AndWhere((Korisnik k) => k.Password == password)
               .Return<Korisnik>("k");

            var results = query.Results;
            Korisnik[] kor = results.ToArray<Korisnik>();

            if (kor.Length == 1)
                return true;
            else
                return false;
        }

        public bool checkUserName(string name)
        {
            var query = client.Cypher
               .Match("(k:Korisnik)")
               .Where((Korisnik k) => k.Name == name)
               .Return<Korisnik>("k");

            var results = query.Results;
            Korisnik[] kor = results.ToArray<Korisnik>();

            if (kor.Length == 1)
                return true;
            else
                return false;
        }

        public bool createUser(string name, string password, string id)
        {
            if (!checkUser(name, password))
            {
                Random rnd = new Random();
                var k = new Korisnik { Name = name, Age = "empty", Pol = "empty", Id = id, Password = rc4.Crypt(password) };
                client.Cypher
                    .Create("(k:Korisnik {newKorisnik})")
                    .WithParam("newKorisnik", k)
                    .ExecuteWithoutResultsAsync()
                    .Wait();
                return true;
            }

            return false;
        }

        public void createRelationshipJeLjubitelj(string id, string companyName)
        {
            client.Cypher
                .Match("(k1:Korisnik)", "(komp1:Company)")
                .Where((Korisnik k1) => k1.Id == id)
                .AndWhere((Company komp1) => komp1.CompanyName == companyName)
                .Create("(k1)-[:JE_LJUBITELJ]->(komp1)")
                .ExecuteWithoutResultsAsync()
                .Wait();
        }

        public void createRelationshipJeKlijent(string id, string rentName)
        {
            client.Cypher
                .Match("(k1:Korisnik)", "(rent1:Rentacar)")
                .Where((Korisnik k1) => k1.Id == id)
                .AndWhere((Rentacar rent1) => rent1.Ime == rentName)
                .Create("(k1)-[:JE_KLIJENT]->(rent1)")
                .ExecuteWithoutResultsAsync()
                .Wait();
        }

        public Korisnik getUserById(string id)
        {
            var query = client.Cypher
               .Match("(k:Korisnik)")
               .Where((Korisnik k) => k.Id == id)
               .Return<Korisnik>("k");

            var results = query.Results;
            Korisnik[] kor = results.ToArray<Korisnik>();

            if (kor[0] != null)
                return kor[0];
            return null;
        }

        public Korisnik getUserByUsernamePassword(string username, string password)
        {
            password = rc4.Crypt(password);
            var query = client.Cypher
               .Match("(k:Korisnik)")
               .Where((Korisnik k) => k.Name == username)
               .AndWhere((Korisnik k) => k.Password == password)
               .Return<Korisnik>("k");

            var results = query.Results;
            Korisnik[] kor = results.ToArray<Korisnik>();

            if (kor[0] != null)
                return kor[0];
            return null;
        }
        
        public void Active(bool active, string userID)
        {
            if (active == true)
            {
                client.Cypher
                    .Match("(a:Aktivan)", "(k1:Korisnik)")
                    .Where((Korisnik k1) => k1.Id == userID)
                    .Create("(a)-[:JE_AKTIVAN]->(k1)")
                    .ExecuteWithoutResultsAsync()
                    .Wait();
            }
            else
            {
                client.Cypher
                    .Match("(a:Aktivan)-[r]->(k1:Korisnik)")
                    .Where((Korisnik k1) => k1.Id == userID)
                    .Delete("r")
                    .ExecuteWithoutResultsAsync()
                    .Wait();
            }
        }

        public CompanyLiking[] getAllCompaniesWithLiking(string idUser)
        {
            var queryKompanije = client.Cypher
                .Match("(komp:Company)", "(k1:Korisnik)")
                .Where((Korisnik k1) => k1.Id == idUser)
                .With("komp, (k1)-[:JE_LJUBITELJ]->(komp) as liked")
                .Return(() => new
                {
                    collection = Return.As<Company>("komp"),
                    liked = Return.As<string>("liked")
                });

            var resultsCompany = queryKompanije.Results;
            CompanyLiking[] kompArray = new CompanyLiking[resultsCompany.Count()];
            for (int i = 0; i < kompArray.Count(); i++)
                kompArray[i] = new CompanyLiking() { Comp = new Company() };

            for(int i=0;i<kompArray.Count();i++)
            {
                kompArray[i].Comp = resultsCompany.ElementAt(i).collection;
                
                if(resultsCompany.ElementAt(i).liked == "[]")
                    kompArray[i].IsLiked = false;
                else
                    kompArray[i].IsLiked = true;
            }


            return kompArray;
        }


        public RentacarLiking[] getAllRentacarsWithLiking(string idUser)
        {
            var queryKompanije = client.Cypher
                .Match("(rent:Rentacar)", "(k1:Korisnik)")
                .Where((Korisnik k1) => k1.Id == idUser)
                .With("rent, (k1)-[:JE_KLIJENT]->(rent) as liked")
                .Return(() => new
                {
                    collection = Return.As<Rentacar>("rent"),
                    liked = Return.As<string>("liked")
                });

            var resultsCompany = queryKompanije.Results;
            RentacarLiking[] rentArray = new RentacarLiking[resultsCompany.Count()];
            for (int i = 0; i < rentArray.Count(); i++)
                rentArray[i] = new RentacarLiking() { Rent = new Rentacar() };

            for (int i = 0; i < rentArray.Count(); i++)
            {
                rentArray[i].Rent = resultsCompany.ElementAt(i).collection;

                if (resultsCompany.ElementAt(i).liked == "[]")
                    rentArray[i].IsLiked = false;
                else
                    rentArray[i].IsLiked = true;
            }

            return rentArray;
        }


        public KorisnikTabela[] getUsersForTable(string idUser)
        {
            var query = client.Cypher
                .Match("(k1:Korisnik)", "(a:Aktivan)","(k2:Korisnik)")
                .Where((Korisnik k2) => k2.Id == idUser)
                .With("k1, collect([size((k1)-[:PRATI]->(:Korisnik)),size((k1)<-[:PRATI]-(:Korisnik))]) as collection, (a)-[:JE_AKTIVAN]->(k1) as aktivan, (k2)-[:PRATI]->(k1) as vecPrati")
                .Return((k1) => new
                {
                    user = k1.As<Korisnik>(),
                    Collection = Return.As<IEnumerable<IEnumerable<string>>>("collection"),
                    aktivan = Return.As<string>("aktivan"),
                    vecPrati = Return.As<string>("vecPrati")
                });

            var ret = query.Results;
            KorisnikTabela[] retData = new KorisnikTabela[ret.Count()];
            for (int count = 0; count < ret.Count(); count++)
                retData[count] = new KorisnikTabela() { user = new Korisnik() };

            for (int i = 0; i < retData.Count(); i++)
            {
                retData[i].User = new Korisnik();
                retData[i].User.Name = ret.ElementAt(i).user.Name;
                retData[i].User.Age = ret.ElementAt(i).user.Age;
                retData[i].User.Id = ret.ElementAt(i).user.Id;
                retData[i].Following = ret.ElementAt(i).Collection.ElementAt(0).ElementAt(0);
                retData[i].Followers = ret.ElementAt(i).Collection.ElementAt(0).ElementAt(1);

                if (ret.ElementAt(i).aktivan == "[]")
                    retData[i].IsActive = false;
                else
                    retData[i].IsActive = true;

                if (ret.ElementAt(i).vecPrati == "[]")
                    retData[i].AlreadyFollow = false;
                else
                    retData[i].AlreadyFollow = true;
            }

            return retData;
        }

        public void createRelationshipPrati(string fromID, string toID)
        {
            client.Cypher
                .Match("(k1:Korisnik)", "(k2:Korisnik)")
                .Where((Korisnik k1) => k1.Id == fromID)
                .AndWhere((Korisnik k2) => k2.Id == toID)
                .Create("(k1)-[:PRATI]->(k2)")
                .ExecuteWithoutResultsAsync()
                .Wait();
        }

        public void createRelationshipIznajmljivanje(string idUser, string idRent, string idAutomobila, string iznajmljenZaDan)
        {

            VezaIznajmio v = new VezaIznajmio { IdAutomobila = idAutomobila, zaDan = iznajmljenZaDan };
            //veza izmedju usera i rentacara koja sadrzi id automobila i datum za koji je iznajmljen automobil
            client.Cypher
                .Match("(k1:Korisnik)", "(rent1:Rentacar)")
                .Where((Korisnik k1) => k1.Id == idUser)
                .AndWhere((Rentacar rent1) => rent1.Id == idRent)
                .Create("(k1)-[:IZNAJMIO {params}]->(rent1)")
                .WithParam("params", v)
                .ExecuteWithoutResultsAsync()
                .Wait();
        }

        public RecommandedRentacar[] getRecommandedItems(string id)
        {
            //vraca rentacar koji ima naveci broj pracenja od strane prijatelja korisnika
            RecommandedRentacar[] returnArray = new RecommandedRentacar[3];

            var query = client.Cypher
                .Match("(k1:Korisnik)", "(rent1:Rentacar)")
                .Where((Korisnik k1) => k1.Id == id)
                .With("rent1, size( (k1)-[:PRATI]->(:Korisnik)-[:JE_KLIJENT]->(rent1) ) as numOfFriends")
                .OrderByDescending("numOfFriends")
                .Return((rent1) => new
                {
                    NumOFFriends = Return.As<string>("numOfFriends"),
                    Rentacar = rent1.As<Rentacar>()
                });

            var ret = query.Results;

            returnArray = new RecommandedRentacar[ret.Count()];
            for (int count = 0; count < ret.Count(); count++)
                returnArray[count] = new RecommandedRentacar() { Rent = new Rentacar() };

            for(int i = 0; i < returnArray.Count(); i++)
            {
                returnArray[i].Rent = new Rentacar();
                returnArray[i].Rent = ret.ElementAt(i).Rentacar;
                returnArray[i].NumberOfFriends = ret.ElementAt(i).NumOFFriends;
            }

            

            return returnArray;
        }

    }
}
