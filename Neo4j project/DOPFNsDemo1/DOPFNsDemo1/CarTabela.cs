﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOPFNsDemo1
{
    class CarTabela
    {
        Car car;
        string cena;
        string marka;

        public Car Car { get => car; set => car = value; }
        public string Cena { get => cena; set => cena = value; }
        public string Marka { get => marka; set => marka = value; }

        public CarTabela() { }
    }
}
