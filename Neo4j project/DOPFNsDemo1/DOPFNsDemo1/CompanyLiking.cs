﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOPFNsDemo1
{
    class CompanyLiking
    {
        Company comp;
        bool isLiked;

        public bool IsLiked { get => isLiked; set => isLiked = value; }
        public Company Comp { get => comp; set => comp = value; }

        public CompanyLiking() { comp = new Company(); }
    }
}
